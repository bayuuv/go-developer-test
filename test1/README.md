# Go Developer Test

This test is intended for prospective Go Developers to join PT Lussa Teknologi Global. You need to do this test first before joining our team. Happy Coding and Good Luck!

## How To

- Type `go run 1.go` for the solution problem number 1. 
- Type `go run 2.go` for the solution problem number 2. 
- Type `go run 3.go` for the solution problem number 3. 
- Type `go run 4.go` for the solution problem number 1. 