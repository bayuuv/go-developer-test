package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	"os"
	"sort"
	"strings"
	"text/tabwriter"
)

func sortArr(arr []int64) {
	var arrInt []int
	for _, value := range arr {
		if value != 0 && value != 1 && value != 2 {
			fmt.Println("Angka yang diinput harus 0, 1 atau 2")
			return
		}
		arrInt = append(arrInt, int(value))
	}
	sort.Ints(arrInt)
	fmt.Println(arrInt)
	fmt.Println()
}

func help() {
	w := tabwriter.NewWriter(os.Stdout, 1, 1, 4, ' ', tabwriter.TabIndent|tabwriter.StripEscape)
	fmt.Fprintln(w, " Usage:")
	fmt.Fprintln(w, "    <command> <array of integer>")
	fmt.Fprintln(w, "")
	fmt.Fprintln(w, " The commands are:")
	fmt.Fprintln(w, "    Sort the array in linear time and using constant space. input array containing only 0's, 1's and 2's. \tExample sort [0,1,2,2,1,0,0,2,0,1,1,0]")
	fmt.Fprintln(w, "    exit \tClose Application")
	fmt.Fprintln(w, "")
	fmt.Fprintln(w, "")
}

func runApp(command string) {
	command = strings.TrimSuffix(command, "\n")
	arrCommandStr := strings.Fields(command)
	var arr []int64

	if len(arrCommandStr) == 2 {
		err := json.Unmarshal([]byte(arrCommandStr[1]), &arr)
		if err != nil {
			fmt.Println(`try "help" for get list of command`)
			return
		}
	} else if arrCommandStr[0] != "help" && arrCommandStr[0] != "exit" {
		fmt.Println(`try "help" for get list of command`)
		return
	}

	switch arrCommandStr[0] {
	case "sort":
		sortArr(arr)
		return
	case "help":
		help()
		return
	case "exit":
		os.Exit(0)
		return
	default:
		output := `try "help" for get list of command`
		fmt.Println(output)
		return
	}
}

func main() {
	reader := bufio.NewReader(os.Stdin)
	for {
		fmt.Print("Hai $ ")
		cmdString, err := reader.ReadString('\n')
		if err != nil {
			fmt.Fprintln(os.Stderr, err)
		}
		runApp(cmdString)
	}
}
