package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	"os"
	"strings"
	"text/tabwriter"
)

func repetition(arr []int64) {
	var resultArr [][]int64

	for i := 0; i < len(arr); i++ {
		for j := i; j < len(arr); j++ {
			var result []int64
			result = append(result, arr[i])
			result = append(result, arr[j])
			resultArr = append(resultArr, result)
		}
	}
	fmt.Println(resultArr)
	fmt.Println()
}

func help() {
	w := tabwriter.NewWriter(os.Stdout, 1, 1, 4, ' ', tabwriter.TabIndent|tabwriter.StripEscape)
	fmt.Fprintln(w, " Usage:")
	fmt.Fprintln(w, "    <command> <array of integer>")
	fmt.Fprintln(w, "")
	fmt.Fprintln(w, " The commands are:")
	fmt.Fprintln(w, "    Find all distinct combinations of given length where repetition of elements is allowed. \tExample repetition [1,2,3]")
	fmt.Fprintln(w, "    exit \tClose Application")
	fmt.Fprintln(w, "")
	fmt.Fprintln(w, "")
}

func runApp(command string) {
	command = strings.TrimSuffix(command, "\n")
	arrCommandStr := strings.Fields(command)
	var arr []int64

	if len(arrCommandStr) == 2 {
		err := json.Unmarshal([]byte(arrCommandStr[1]), &arr)
		if err != nil {
			fmt.Println(`try "help" for get list of command`)
			return
		}
	} else if arrCommandStr[0] != "help" && arrCommandStr[0] != "exit" {
		fmt.Println(`try "help" for get list of command`)
		return
	}

	switch arrCommandStr[0] {
	case "repetition":
		repetition(arr)
		return
	case "help":
		help()
		return
	case "exit":
		os.Exit(0)
		return
	default:
		output := `try "help" for get list of command`
		fmt.Println(output)
		return
	}
}

func main() {
	reader := bufio.NewReader(os.Stdin)
	for {
		fmt.Print("Hai $ ")
		cmdString, err := reader.ReadString('\n')
		if err != nil {
			fmt.Fprintln(os.Stderr, err)
		}
		runApp(cmdString)
	}
}
