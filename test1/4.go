package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	"os"
	"strings"
	"text/tabwriter"
)

func maxProduct(arr []int64) {
	var current int64 = 1
	var min int64 = 1
	var max int64 = arr[0]
	var resultArr []int64

	for i := 0; i < len(arr); i++ {
		switch {
		case arr[i] > 0:
			current, min = arr[i]*current, arr[i]*min

		case arr[i] < 0:
			current, min = arr[i]*min, arr[i]*current

		default:
			current = 0
			min = 1
		}

		if max < current {
			max = current
			resultArr = append(resultArr, arr[i])
		}

		if current <= 0 {
			current = 1
		}
	}

	fmt.Println("the maximum product sub-array is", resultArr, "having product", max)
	fmt.Println()
}

func help() {
	w := tabwriter.NewWriter(os.Stdout, 1, 1, 4, ' ', tabwriter.TabIndent|tabwriter.StripEscape)
	fmt.Fprintln(w, " Usage:")
	fmt.Fprintln(w, "    <command> <array of integer>")
	fmt.Fprintln(w, "")
	fmt.Fprintln(w, " The commands are:")
	fmt.Fprintln(w, "    Given an array of integers, find maximum product (multiplication) subarray. In other words, find sub-array that has maximum product of its elements. \tExample max [-6,4,-5,8,-10,0,8]")
	fmt.Fprintln(w, "    exit \tClose Application")
	fmt.Fprintln(w, "")
	fmt.Fprintln(w, "")
}

func runApp(command string) {
	command = strings.TrimSuffix(command, "\n")
	arrCommandStr := strings.Fields(command)
	var arr []int64

	if len(arrCommandStr) == 2 {
		err := json.Unmarshal([]byte(arrCommandStr[1]), &arr)
		if err != nil {
			fmt.Println(`try "help" for get list of command`)
			return
		}
	} else if arrCommandStr[0] != "help" && arrCommandStr[0] != "exit" {
		fmt.Println(`try "help" for get list of command`)
		return
	}

	switch arrCommandStr[0] {
	case "max":
		maxProduct(arr)
		return
	case "help":
		help()
		return
	case "exit":
		os.Exit(0)
		return
	default:
		output := `try "help" for get list of command`
		fmt.Println(output)
		return
	}
}

func main() {
	reader := bufio.NewReader(os.Stdin)
	for {
		fmt.Print("Hai $ ")
		cmdString, err := reader.ReadString('\n')
		if err != nil {
			fmt.Fprintln(os.Stderr, err)
		}
		runApp(cmdString)
	}
}
