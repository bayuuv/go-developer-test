# Go Developer Test

This test is intended for prospective Go Developers to join PT Lussa Teknologi Global. You need to do this test first before joining our team. Happy Coding and Good Luck!

## How To

- Type `go run main.go`
- Open Postman anda Type `http://localhost:8080/`